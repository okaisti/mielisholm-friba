import { createMuiTheme, Theme } from '@material-ui/core/styles'

const theme: Theme = createMuiTheme({
  palette: {
    primary: { main: '#11cb5f' },
    secondary: { main: '#11cb5f' }
  }
})

export default theme
