import React from 'react'
import { NextPage } from 'next'
import { Button, Grid, Typography } from '@material-ui/core'

const Index: NextPage = () => (
  <>
    <Grid container justify="center" direction="column" alignItems="center">
      <Grid item>
        <Typography variant="h1">Mielisholm Friba</Typography>
      </Grid>
      <Grid item>
        <Button variant="contained" color="primary">
          Go!
        </Button>
      </Grid>
    </Grid>
  </>
)

export default Index
