import * as React from 'react'
import { AppProps } from 'next/app'
import { NextPage } from 'next'
import Head from 'next/head'
import { ThemeProvider } from '@material-ui/core/styles'
import theme from 'src/theme'

const App: NextPage<AppProps> = ({ Component, pageProps }) => (
  <ThemeProvider theme={theme}>
    <Head>
      <title>Mielisholm Friba</title>
    </Head>
    <Component {...pageProps} />
  </ThemeProvider>
)

export default App
