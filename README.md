# Mielisholm friba

## Links

- [Production site](https://mielisholm.now.sh/)
- [Firebase dashboard](https://console.firebase.google.com/u/0/project/mielisholm-friba/overview)
- [Zeit dashboard](https://zeit.co/oliverkaisti/mielisholm-friba)

## Features

- Bootstrapped with `create-next-app`
- CD pipeline with Zeit Now
- Code formatting and linting in pre-commit hook
  ...
